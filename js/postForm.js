const requestURL = "https://ajax.test-danit.com/api/v2/cards/login";

function sendRequest(method, url, data) {
    return fetch(url, {
        method: method,
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(data)
    })
        .then(response => response.text())
        .then(response =>{
             if (response != 0) {
                localStorage.setItem('token', response.data.token);
                 sessionStorage.setItem('email', response.data.email);
                 setTimeout(function () {
                     alert('Вітаємо знову!');
                 }, 500);

             } else  {
                alert(response.message);
             }
        })
}


let data = {email: 'qaz@qasz.qaz', password: 'qazxsw'};

sendRequest('POST', requestURL, data);

