const login_form = document.getElementById('login-form');
const login_btn = document.getElementById('login-btn');
login_btn.addEventListener('click', function(event){
    login_form.style.display = 'flex';
});


const filter = document.getElementById('filter');
const request_btn = document.getElementById('request-btn');
request_btn.addEventListener('click', function(event){
    create_card__btn.style.display = "block";
    login_btn.style.display = "none";
    login_form.style.display = "none";
    filter.style.display = "flex";
});


const create_card = document.getElementById('create-card');
const create_card__btn = document.getElementById('create-card__btn');
create_card__btn.addEventListener('click', function(event){
    create_card.style.display = "block";
});


const kardiolog = document.getElementById('kardiolog');
const kardiolog_btn = document.getElementById('kardiolog-btn');
kardiolog_btn.addEventListener('click', function(event){
    kardiolog.style.display = "flex";
    stomatolog.style.display = "none";
    terapevt.style.display = "none";
});
const stomatolog = document.getElementById('stomatolog');
const stomatolog_btn = document.getElementById('stomatolog-btn');
stomatolog_btn.addEventListener('click', function(event){
    stomatolog.style.display = "flex";
    kardiolog.style.display = "none";
    terapevt.style.display = "none";
});
const terapevt = document.getElementById('terapevt');
const terapevt_btn = document.getElementById('terapevt-btn');
terapevt_btn.addEventListener('click', function(event){
    terapevt.style.display = "flex";
    stomatolog.style.display = "none";
    kardiolog.style.display = "none";
});

//
const kardiolog_input = document.getElementsByClassName('kardiolog-input');
const kardiolog_input_btn = document.getElementById('kardiolog-input__btn');
kardiolog_input_btn.addEventListener('click', function(event){
    for(let i = 0; i < kardiolog_input.length; ++i)  console.log((kardiolog_input[i]).value);
    create_visit();
    kardiolog.style.display = "none";
});
document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("kardiolog-input__btn").onclick = function() {
      console.log((document.getElementById("doctor-select").value));
    };
});

const stomatolog_input = document.getElementsByClassName('stomatolog-input');
const stomatolog_input_btn = document.getElementById('stomatolog-input__btn');
stomatolog_input_btn.addEventListener('click', function(event){
    for(let i = 0; i < stomatolog_input.length; ++i)  console.log((stomatolog_input[i]).value);
    create_visit();
    stomatolog.style.display = "none";
});
document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("stomatolog-input__btn").onclick = function() {
      console.log((document.getElementById("doctor-select").value));
    };
});

const terapevt_input = document.getElementsByClassName('terapevt-input');
const terapevt_input_btn = document.getElementById('terapevt-input__btn');
terapevt_input_btn.addEventListener('click', function(event){
    for(let i = 0; i < terapevt_input.length; ++i)  console.log((terapevt_input[i]).value);
    create_visit();
    terapevt.style.display = "none";
});
document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("terapevt-input__btn").onclick = function() {
      console.log((document.getElementById("doctor-select").value));
    };
});

//
function create_visit(){
    const div = document.createElement('div');
    div.id = "visitcard", "container";
    div.className = "visitcard";
    div.style.width = "300px";
    div.style.marginLeft = "117px";
    div.style.marginTop = "10px";
    div.style.border = "1px solid gray";
    document.body.append(div);

    const p1 = document.createElement('p');
    p1.id = "visitcard-doctor-value";
    p1.style.margin = "5px";
    p1.textContent = '';
    div.append(p1);

    const p2 = document.createElement('p');
    p2.id = "visitcard-fio-value";
    p2.style.margin = "5px";
    p2.textContent = '';
    div.append(p2);

    const hr = document.createElement('hr');
    hr.style.width = "200px";
    div.append(hr);

    const btn1 = document.createElement('button');
    btn1.className = "small-card__btn";
    btn1.textContent = 'Показать больше';
    btn1.style.width = "150px";
    btn1.style.margin = "5px";
    div.append(btn1);

    const btn2 = document.createElement('button');
    btn2.className = "small-card__btn";
    btn2.textContent = 'Редактировать';
    btn2.style.width = "150px";
    btn2.style.margin = "5px";
    div.append(btn2);
}



